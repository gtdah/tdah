.. TDAH documentation master file, created by
   sphinx-quickstart on Mon Sep 24 20:11:58 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


===========================================================================
TDAH (Trouble du Déficit de l'Attention avec ou sans Hyperactivité)
===========================================================================

.. sidebar:: TDAH

    :Date: |today|
    :AuteurE()s: **tdah AT riseup.net**
    :Pour: TouTEs

    - https://twitter.com/hypersupers
    - https://twitter.com/TDAH_Partout
    - https://www.tdah-france.fr/



.. toctree::
   :maxdepth: 4


   actions/actions
   people/people


