
.. index::
   pair: Docteure ; Annick Vincent

.. _annick_vincent:

===========================================================================
Annick Vincent (Québec)
===========================================================================

.. seealso::

   - https://www.amazon.fr/Annick-Vincent/e/B00KPFCIIC/ref=dp_byline_cont_book_1


.. contents::
   :depth: 3


Colloques
===========

2018
------

.. seealso::

   - :ref:`annick_vincent_2018`
   - :ref:`annick_vincent_2018_2`



