
.. index::
   pair: Médecins ; Olivier Revol

.. _olivier_revol:

===========================================================================
Olivier Revol (Lyon)
===========================================================================

.. seealso::

   - https://www.decitre.fr/auteur/351587/Olivier+Revol


.. contents::
   :depth: 3


Colloques
===========

2018
------

.. seealso::

   - :ref:`olivier_revol_2018`



